import '~/styles/globals.css'

import { Analytics } from '@vercel/analytics/react'
import { SpeedInsights } from '@vercel/speed-insights/next'
import { Inter } from 'next/font/google'

import { me } from '~/me'

const inter = Inter({
  subsets: ['latin'],
  variable: '--font-sans',
})

export const metadata = {
  title: me.name,
  description: `${me.name} | ${me.description}`,
  icons: [{ rel: 'icon', type: 'image/svg+xml', url: '/favicon.svg' }],
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={`bg-sky-950 font-sans text-white print:bg-white print:text-sky-950 ${inter.variable}`}>
        <Analytics />
        <SpeedInsights />
        {children}
      </body>
    </html>
  )
}
