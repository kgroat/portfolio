import { me } from '~/me'

export const GET = () => {
  return new Response(
    JSON.stringify(me, null, 2),
    { headers: { 'Content-Type': 'application/json' } },
  )
}
