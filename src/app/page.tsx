/* eslint-disable @next/next/no-img-element */
import Image from 'next/image'
import Link from 'next/link'

import {
  type Job,
  me,
  type Link as MeLink,
  type Project,
  type Responsibility,
  type Skill,
} from '~/me'

function Pill({ children }: { children: string }) {
  return (
    <span className='rounded-full bg-sky-800 px-2 py-1 text-sm text-white'>
      {children}
    </span>
  )
}

function JobResponsibility({ resp }: { resp: Responsibility }) {
  return (
    <div className='mb-4'>
      <h4 className='text-lg font-bold'>{resp.title}</h4>
      <p className='text-justify'>{resp.description}</p>
      <div className='mt-1 flex flex-row flex-wrap gap-2'>
        {resp.technologies.map((tech) => (
          <Pill key={tech}>{tech}</Pill>
        ))}
      </div>
    </div>
  )
}

function JobItem({ job }: { job: Job }) {
  const start = job.startDate.toLocaleDateString('en-us', {
    month: 'long',
    year: 'numeric',
  })
  const end =
    job.endDate?.toLocaleDateString('en-us', {
      month: 'long',
      year: 'numeric',
    }) ?? 'Present'

  return (
    <div className='my-2 break-inside-avoid-page'>
      <div className='sticky top-0 mb-2 border-b-[1px] border-white bg-sky-950 print:border-sky-950 print:bg-white'>
        <h3 className='text-xl font-bold'>{job.company}</h3>
        <p>
          {start} - {end}
        </p>
      </div>
      {job.responsibilities.map((resp) => (
        <JobResponsibility key={resp.title} resp={resp} />
      ))}
    </div>
  )
}

function ProjectItem({ proj }: { proj: Project }) {
  return (
    <div className='pt-2'>
      <h3 className='sticky top-0 mb-2 border-b-[1px] border-white bg-sky-950 text-xl font-bold print:border-sky-950 print:bg-white'>
        {
          proj.url
          ? <Link
              className='text-cyan-400 print:text-cyan-800'
              href={proj.url}
              target='_blank'
            >
              {proj.name}
            </Link>
          : <span>
              {proj.name}
            </span>
        }
      </h3>
      <div className='text-justify'>{proj.description}</div>
      {proj.repository && (
        <Link
          className='text-cyan-400 print:hidden'
          href={proj.repository}
          target='_blank'
        >
          See the code
        </Link>
      )}
      <div className='mt-1 flex flex-row flex-wrap gap-2 pb-2'>
        {proj.technologies.map((tech) => (
          <Pill key={tech}>{tech}</Pill>
        ))}
      </div>
    </div>
  )
}

function LinkItem({ link }: { link: MeLink }) {
  if (link.printImg && link.img) {
    return (
      <>
        <Link
          className='m-2 flex items-center print:hidden'
          href={link.url}
          target='_blank'
        >
          <Image src={link.img} alt={link.name} width={50} height={50} />
        </Link>
        <Link
          className='m-2 hidden items-center print:flex'
          href={link.url}
          target='_blank'
        >
          <Image src={link.printImg} alt={link.name} width={50} height={50} />
        </Link>
      </>
    )
  } else if (link.img) {
    return (
      <Link className='m-2 flex items-center' href={link.url} target='_blank'>
        <Image src={link.img} alt={link.name} width={50} height={50} />
      </Link>
    )
  } else {
    return (
      <Link className='m-2 flex items-center' href={link.url}>
        {link.name}
      </Link>
    )
  }
}

function SkillItem({ skill }: { skill: Skill }) {
  return (
    <div id={`skills-${skill.name}`} className='w-full grow break-inside-avoid pb-1 sm:w-[48%] lg:w-[30%]'>
      <h3
        className='sticky top-0 mb-2 flex items-center gap-2 border-b-[1px] border-white bg-sky-950 py-1 text-xl font-bold print:border-sky-950 print:bg-white'
      >
        <img
          className='inline-block size-6'
          src={`https://go-skill-icons.vercel.app/api/icons?i=${skill.icon}&theme=light`}
          alt={`${skill.name} icon`}
        />
        {skill.name}
      </h3>
      <div className='text-justify'>{skill.description}</div>
    </div>
  )
}

export default function HomePage() {
  return (
    <main
      id='top'
      className='flex min-h-screen flex-col items-center justify-center p-8 print:p-0'
    >
      <Image
        src={me.image}
        alt={me.name}
        width={300}
        height={300}
        className='mt-16 rounded-full'
      />
      <h1 className='py-4 text-5xl font-bold'>
        <Link
          className='text-cyan-400 print:hidden'
          href={`mailto:${me.email}`}
          target='_blank'
        >
          {me.name}
        </Link>
        <Link
          className='hidden text-cyan-800 print:inline'
          href={me.url}
          target='_blank'
        >
          {me.name}
        </Link>
      </h1>
      <Link
        href='/resume.pdf'
        target='_blank'
        download='resume.pdf'
        className='mt-1 text-cyan-400 print:hidden print:text-cyan-800'
      >
        Download as a PDF
      </Link>
      <div className='mt-6 flex flex-row justify-center'>
        {me.links.map((link) => (
          <LinkItem key={link.name} link={link} />
        ))}
      </div>
      <div className='mb-10' />
      <div className='flex flex-col gap-4 md:w-[80vw] md:flex-row lg:w-[75vw] xl:w-[70vw]'>
        <div className='flex shrink flex-col'>
          <p className='break-after-page text-justify text-lg'>{me.description}</p>
          <h2 id='experience' className='my-4 text-3xl font-bold'>
            Experience
          </h2>
          {me.experience.map((job) => (
            <JobItem key={job.company} job={job} />
          ))}
          <div className='break-after-page' />
          <h2 id='projects' className='my-4 text-3xl font-bold'>
            Projects
          </h2>
          {me.projects.map((proj) => (
            <ProjectItem key={proj.name} proj={proj} />
          ))}
          <div className='break-after-page print:hidden' />
          <h2 id='skills' className='my-4 text-3xl font-bold print:hidden'>
            Skills
          </h2>
          <div className='flex flex-row flex-wrap gap-4 print:hidden'>
            {me.skills.map((skill) => (
              <SkillItem key={skill.name} skill={skill} />
            ))}
          </div>

          <div className='mt-4 flex flex-col items-center print:hidden'>
            <Link
              className='my-1 text-cyan-400 print:text-cyan-800'
              href={`mailto:${me.email}`}
              target='_blank'
            >
              Contact me
            </Link>
            <Link
              className='my-1 text-cyan-400 print:text-cyan-800'
              href='/resume.pdf'
              target='_blank'
              download='resume.pdf'
            >
              Download as a PDF
            </Link>
            <Link
              className='my-1 text-cyan-400 print:text-cyan-800'
              href='https://gitlab.com/kgroat/portfolio'
              target='_blank'
            >
              See the code
            </Link>
            <Link
              className='my-1 text-cyan-400 print:text-cyan-800'
              href='/api/me'
              target='_blank'
            >
              See the JSON version of my resume
            </Link>
          </div>
        </div>
        <div className='sticky inset-x-0 bottom-0 min-w-32 flex-col md:flex print:hidden'>
          <div className='-mt-4 h-8 bg-gradient-to-t from-sky-950 to-transparent md:hidden'></div>
          <nav className='scrollbar-none sticky top-0 flex max-h-[100vh] flex-row flex-wrap justify-center overflow-y-auto bg-sky-950 pb-4 md:-mt-4 md:justify-start md:pt-4'>
            <Link className='mr-4 text-cyan-400 print:text-cyan-800' href='#top'>
              Top
            </Link>
            <Link className='mr-4 text-cyan-400 print:text-cyan-800' href='#experience'>
              Experience
            </Link>
            <Link className='mr-4 text-cyan-400 print:text-cyan-800' href='#projects'>
              Projects
            </Link>
            <Link className='mr-4 text-cyan-400 print:text-cyan-800' href='#skills'>
              Skills
            </Link>
            <hr className='my-2 w-full' />
            <div className='flex flex-row flex-wrap justify-center gap-2'>
              {
                me.skills.map((s) => (
                  <a key={s.name} className='w-5 md:w-9' href={`#skills-${s.name}`}>
                    <img
                      src={`https://go-skill-icons.vercel.app/api/icons?i=${s.icon}&theme=light`}
                      alt={`${s.name} icon`}
                    />
                  </a>
                ))
              }
            </div>
          </nav>
        </div>
      </div>
    </main>
  )
}
