# Kevin Groat's Portfolio

## Quickstart

In order to get started, you will need [`NodeJS`](https://nodejs.org/en) and [`pnpm`](https://pnpm.io/) installed.  I suggest using [`asdf`](https://asdf-vm.com/) version manager, though this is not a requirement.

If you are using `asdf`, all you need to do to install these prerequisites is run the following commands:
* `asdf plugin-add nodejs` - if you don't already have the `nodejs` plugin installed.
* `asdf plugin-add pnpm` - if you don't already have the `pnpm` plugin installed.
* `asdf install` - installs the requisite versions of both of these tools.

To see which versions of these tools will be installed, reference the [`.tool-versions`](./.tool-versions) file.

Once you have the right tools, you'll need to install the dependencies using `pnpm install`.

## Modifying the data to fit you

If you want to use this repository as a reference, all you need to do to update it to match your experience is update [`src/me.ts`](./src/me.ts).  This file is the data which is used to generate all of the HTML.  If you want to customize the look and feel, check out [`src/app/page.tsx`](./src/app/page.tsx).

## Start the dev server

To start the dev server, all you need to do is run `pnpm run dev`.  This will start the `Next.js` server in dev mode, and you can make changes and get hot module reloading out-of-the-box for free.

## Building

To build the final assets, all you need to do is run `pnpm run build`.  This will tell `Next.js` to build the final assets, and will then use `puppeteer` to generate a PDF of those assets so it can be downloaded.

## Deploying

If you have a Vercel account, all you need to do is wire it up to read from the repository, and you should be good to go.
