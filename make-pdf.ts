import { launch } from 'puppeteer'

const MARGIN = 64

const sleep = (ms: number) => new Promise<void>(resolve => setTimeout(resolve, ms))

const bin = process.env.CHROME_DIR
  ? `${process.env.CHROME_DIR}/bin/google-chrome-stable`
  : undefined

async function main() {
  const browser = await launch({
    executablePath: bin,
    headless: true,
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
  })
  const page = await browser.newPage()
  await page.goto('http://localhost:3000/', { waitUntil: 'networkidle2' })
  await page.emulateMediaType('print')
  await page.evaluate(() => {
    document.body.classList.add('pdf')
  })
  await sleep(1000)
  await page.pdf({
    path: 'public/resume.pdf',
    margin: {
      top: MARGIN,
      left: MARGIN,
      right: MARGIN,
      bottom: MARGIN,
    },
    format: 'Letter',
  })

  await browser.close()
}

main().catch(err => {
  console.error('Could not export pdf:', err)
  process.exit(1)
})
